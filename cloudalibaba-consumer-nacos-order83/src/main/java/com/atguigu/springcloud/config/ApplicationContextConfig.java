package com.atguigu.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author zzyy
 * @version 1.0
 * @create 2020/03/06
 */
@Configuration
public class ApplicationContextConfig {

    /**
     * LoadBalanced：支持自动负载均衡，可以使用服务名访问
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
